<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Generate CRUD</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/milligram/1.4.1/milligram.css">
</head>

<body>
    <div class="container">
        <h1>Generate CRUD</h1>
        <p>Generate CRUD for your Laravel project.</p>
        <p>Fill the form below to generate CRUD.</p>
        @if(session('error'))
        <div class="alert alert-danger">
            {{session('error')}}
        </div>
        @endif
        <form action="{{ route('generate-crud') }}" method="POST">
            @csrf
            <fieldset>
                <label for="model">Model Name</label>
                <input type="text" name="model" id="model" placeholder="Model Name" required>
                <label for="fields">Fields</label>
                <textarea name="fields" id="fields" cols="30" rows="10" placeholder="Fields" required></textarea>
                <input type="submit" value="Generate">
            </fieldset>
        </form>
        <fieldset>
            <label for="field_type">Field Type</label>
            <select name="field_type" id="field_type">
                <option value="text" selected>Text</option>
                <option value="textarea">Textarea</option>
                <option value="date">Date</option>
                <option value="select">Select</option>
                <option value="radio">Radio</option>
                <option value="checkbox">Checkbox</option>
            </select>
            <label for="field_name">Field Name</label>
            <input type="text" name="field_name" id="field_name" placeholder="Field Name">
            <div id="field_option">
                <label for="field_options">Field Options</label>
                <input type="text" name="field_options" id="field_options" placeholder="Field Options">
            </div>
            <button onclick="addfield()">Add Field</button>
        </fieldset>
    </div>
    <script>
        document.querySelector("#field_option").style.display = "none";
        document.querySelector("#field_type").addEventListener("change", function() {
            if (this.value == "select" || this.value == "radio" || this.value == "checkbox") {
                document.querySelector("#field_option").style.display = "block";
            } else {
                document.querySelector("#field_option").style.display = "none";
            }
        });
        function addfield() {
            var field_type = document.querySelector("#field_type").value;
            var field_name = document.querySelector("#field_name").value;
            var field_options = document.querySelector("#field_options").value;
            var fields = document.querySelector("#fields").value;
            if (field_type == "select" || field_type == "radio" || field_type == "checkbox") {
                fields += field_type + ":" + field_name + ":" + field_options + "###";
            } else {
                fields += field_type + ":" + field_name + "###";
            }
            document.querySelector("#fields").value = fields;
            document.querySelector("#field_name").value = "";
            document.querySelector("#field_options").value = "";
        }
    </script>
</body>
</html>