<?php

namespace Dalhaqq\LaravelCrudGenerator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class CrudGeneratorController extends Controller
{
    public function index()
    {
        return view('crudgenerator::index');
    }

    public function generate(Request $request)
    {
        $model_name = $request->model;
        $base_name = strtolower($model_name);
        $table_name = $base_name;
        $controller_name = $model_name.'Controller';
        // check if table name is existing in database
        $tables = DB::select('SHOW TABLES');
        $tables = array_map('current', $tables);
        if (in_array($table_name, $tables)) {
            return redirect()->back()->with('error', 'Model name already exists!');
        }
        $fields = rtrim($request->fields, '#');
        $fields = explode('###', $fields);
        $generated_fields = [];
        foreach ($fields as $field) {
            $field = explode(':', $field);
            $generated_field = [
                'name' => trim($field[1]),
                'column' => str_replace(' ', '_', strtolower(trim($field[1]))),
                'type' => trim($field[0]),
            ];
            if (isset($field[2])) {
                $generated_field['options'] = array_map('trim', explode(',', $field[2]));
            }
            $generated_fields[] = $generated_field;
        }
        $this->generateController($controller_name, $model_name, $base_name);
        $this->generateMigration($table_name, $generated_fields);
        $this->generateModel($model_name, $table_name);
        $this->generateViews($base_name, $generated_fields);
        $this->addRoute($base_name, $controller_name);
        Artisan::call('migrate');
        return redirect('/'.$base_name);
    }

    private function generateController($controller_name, $model_name, $base_name)
    {
        $controller_template = str_replace(
            [
                '{{controller_name}}',
                '{{model_name}}',
                '{{base_name}}',
            ],
            [
                $controller_name,
                $model_name,
                $base_name,
            ],
            $this->getStub('Controller')
        );
        $this->write(app_path("/Http/Controllers/{$controller_name}.php"), $controller_template);
    }

    private function generateFields($fields) {
        $fields_template = '';
        foreach ($fields as $field) {
            switch ($field['type']){
                case 'date':
                    $field['type'] = 'date';
                    break;
                default:
                    $field['type'] = 'string';
                    break;
            }
            $fields_template .= "\t\t\t\$table->{$field['type']}('{$field['column']}')";
            $fields_template .= ";\n";
        }
        return $fields_template;
    }

    private function generateMigration($table_name, $fields)
    {
        $migration_template = str_replace(
            [
                '{{table_name}}',
                '{{fields}}',
            ],
            [
                $table_name,
                $this->generateFields($fields),
            ],
            $this->getStub('Migration')
        );
        $date = date('Y_m_d_His');
        $file_name = "{$date}_create_{$table_name}_table.php";
        $this->write(database_path("/migrations/{$file_name}"), $migration_template);
    }

    private function generateModel($model_name, $table_name)
    {
        $model_template = str_replace(
            [
                '{{model_name}}',
                '{{table_name}}',
            ],
            [
                $model_name,
                $table_name,
            ],
            $this->getStub('Model')
        );
        $this->write(app_path("/Models/{$model_name}.php"), $model_template);
    }

    private function generateViews($base_name, $fields)
    {
        $index_view = $this->generateIndexView($base_name, $fields);
        $create_view = $this->generateCreateView($base_name, $fields);
        $edit_view = $this->generateEditView($base_name, $fields);
        $this->write(resource_path("views/{$base_name}-index.blade.php"), $index_view);
        $this->write(resource_path("views/{$base_name}-create.blade.php"), $create_view);
        $this->write(resource_path("views/{$base_name}-edit.blade.php"), $edit_view);
    }

    private function generateIndexView($base_name, $fields)
    {
        $thead = '<th>No</th>';
        $tbody = '<td>{{ $loop->iteration }}</td>';
        foreach ($fields as $field) {
            $thead .= "<th>$field[name]</th>\n\t\t\t";
            $tbody .= "<td>{{ \$i->{$field['column']} }}</td>\n\t\t\t\t";
        }
        $thead .= "<th>Actions</th>";
        $tbody .= "<td>
        \t\t\t\t\t\t<form id=\"delete-{{ \$i->id }}\" action=\"{{ route('{$base_name}.destroy', \$i->id) }}\" method=\"post\">
        \t\t\t\t\t\t\t@csrf
        \t\t\t\t\t\t\t@method('DELETE')
        \t\t\t\t\t\t</form>
        \t\t\t\t\t\t<a href=\"{{ route('{$base_name}.edit', \$i->id) }}\" class=\"button\">Edit</a>
        \t\t\t\t\t\t\t<button type=\"submit\" class=\"button-outline\" form=\"delete-{{ \$i->id }}\" onclick=\"return confirm('Yakin ingin menghapus data ini?')\">Delete</button>
        \t\t\t\t\t</td>";
        $index_view = str_replace(
            [
                '{{base_name}}',
                '{{thead}}',
                '{{tbody}}',
            ],
            [
                $base_name,
                $thead,
                $tbody,
            ],
            $this->getStub('views/index')
        );
        return $index_view;
    }

    private function generateCreateView($base_name, $fields)
    {
        $form = '';
        foreach ($fields as $field) {
            $form .= "<div class=\"form-group\">\n\t\t\t\t\t";
            $form .= "<label for=\"{$field['column']}\">$field[name]</label>\n\t\t\t\t\t";
            if ($field['type'] == 'text') {
                $form .= "<input type=\"text\" name=\"{$field['column']}\" id=\"{$field['column']}\" class=\"form-control\">\n\t\t\t\t";
            } elseif ($field['type'] == 'textarea') {
                $form .= "<textarea name=\"{$field['column']}\" id=\"{$field['column']}\" class=\"form-control\" cols=\"30\" rows=\"10\"></textarea>\n\t\t\t\t";
            } elseif ($field['type'] == 'select') {
                $form .= "<select name=\"{$field['column']}\" id=\"{$field['column']}\" class=\"form-control\">\n\t\t\t\t\t\t";
                foreach ($field['options'] as $option) {
                    $form .= "<option value=\"{$option}\">{{ __('{$option}') }}</option>\n\t\t\t\t\t\t";
                }
                $form .= "</select>\n\t\t\t\t";
            } elseif ($field['type'] == 'radio') {
                foreach ($field['options'] as $option) {
                    $form .= "<div class=\"form-check\">\n\t\t\t\t\t\t\t";
                    $form .= "<input type=\"radio\" name=\"{$field['column']}\" id=\"{$field['column']}\" class=\"form-check-input\" value=\"{$option}\">\n\t\t\t\t\t\t\t";
                    $form .= "<label for=\"{$field['column']}\" class=\"form-check-label\">{{ __('{$option}') }}</label>\n\t\t\t\t\t\t";
                    $form .= "</div>\n\t\t\t\t\t";
                }
            } elseif ($field['type'] == 'checkbox') {
                foreach ($field['options'] as $option) {
                    $form .= "<div class=\"form-check\">\n\t\t\t\t\t\t\t";
                    $form .= "<input type=\"checkbox\" name=\"{$field['column']}\" id=\"{$field['column']}\" class=\"form-check-input\" value=\"{$option}\">\n\t\t\t\t\t\t\t";
                    $form .= "<label for=\"{$field['column']}\" class=\"form-check-label\">{{ __('{$option}') }}</label>\n\t\t\t\t\t\t";
                    $form .= "</div>\n\t\t\t\t\t";
                }
            } else {
                $form .= "<input type=\"{$field['type']}\" name=\"{$field['column']}\" id=\"{$field['column']}\" class=\"form-control\">\n\t\t\t\t";
            }
            $form .= "</div>\n\t\t\t\t";
        }
        $create_view = str_replace(
            [
                '{{base_name}}',
                '{{form}}',
            ],
            [
                $base_name,
                $form,
            ],
            $this->getStub('views/create')
        );
        return $create_view;
    }

    private function generateEditView($base_name, $fields)
    {
        $form = '';
        foreach ($fields as $field) {
            $form .= "<div class=\"form-group\">\n\t\t\t\t\t";
            $form .= "<label for=\"{$field['column']}\">$field[name]</label>\n\t\t\t\t\t";
            if ($field['type'] == 'text') {
                $form .= "<input type=\"text\" name=\"{$field['column']}\" id=\"{$field['column']}\" class=\"form-control\" value=\"{{ \${$base_name}->{$field['column']} }}\">\n\t\t\t\t";
            } elseif ($field['type'] == 'textarea') {
                $form .= "<textarea name=\"{$field['column']}\" id=\"{$field['column']}\" class=\"form-control\" cols=\"30\" rows=\"10\">{{ \${$base_name}->{$field['column']} }}</textarea>\n\t\t\t\t";
            } elseif ($field['type'] == 'select') {
                $form .= "<select name=\"{$field['column']}\" id=\"{$field['column']}\" class=\"form-control\">\n\t\t\t\t\t\t";
                foreach ($field['options'] as $option) {
                    $form .= "<option value=\"{$option}\" {{ \${$base_name}->{$field['column']} == '{$option}' ? 'selected' : '' }}>{{ __('{$option}') }}</option>\n\t\t\t\t\t\t";
                }
                $form .= "</select>\n\t\t\t\t";
            } elseif ($field['type'] == 'radio') {
                foreach ($field['options'] as $option) {
                    $form .= "<div class=\"form-check\">\n\t\t\t\t\t\t\t";
                    $form .= "<input type=\"radio\" name=\"{$field['column']}\" id=\"{$field['column']}\" class=\"form-check-input\" value=\"{$option}\" {{ \${$base_name}->{$field['column']} == '{$option}' ? 'checked' : '' }}>\n\t\t\t\t\t\t\t";
                    $form .= "<label for=\"{$field['column']}\" class=\"form-check-label\">{{ __('{$option}') }}</label>\n\t\t\t\t\t\t";
                    $form .= "</div>\n\t\t\t\t\t";
                }
            } elseif ($field['type'] == 'checkbox') {
                foreach ($field['options'] as $option) {
                    $form .= "<div class=\"form-check\">\n\t\t\t\t\t\t\t";
                    $form .= "<input type=\"checkbox\" name=\"{$field['column']}
                    []\" id=\"{$field['column']}\" class=\"form-check-input\" value=\"{$option}\" {{ in_array('{$option}', \${$base_name}->{$field['column']}) ? 'checked' : '' }}>\n\t\t\t\t\t\t\t";
                    $form .= "<label for=\"{$field['column']}\" class=\"form-check-label\">{{ __('{$option}') }}</label>\n\t\t\t\t\t\t";
                    $form .= "</div>\n\t\t\t\t\t";
                }
            } else {
                $form .= "<input type=\"{$field['type']}\" name=\"{$field['column']}\" id=\"{$field['column']}\" class=\"form-control\" value=\"{{ \${$base_name}->{$field['column']} }}\">\n\t\t\t\t";
            }
            $form .= "</div>\n\t\t\t\t";
        }
        $edit_view = str_replace(
            [
                '{{base_name}}',
                '{{form}}',
            ],
            [
                $base_name,
                $form,
            ],
            $this->getStub('views/edit')
        );
        return $edit_view;
    }

    private function addRoute($base_name, $controller_name)
    {
        $route = "\nRoute::resource('{$base_name}', 'App\\Http\\Controllers\\{$controller_name}');\n";
        $route_file = file_get_contents(base_path('routes/web.php'));
        $route_file .= $route;
        $this->write(base_path('routes/web.php'), $route_file);
    }

    private function getStub($type)
    {
        return file_get_contents(__DIR__ . "/stubs/$type.stub");
    }

    private function write($path, $content)
    {
        $f = fopen($path, 'w');
        fwrite($f, $content);
        fclose($f);
    }
}
