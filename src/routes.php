<?php

use Dalhaqq\LaravelCrudGenerator\CrudGeneratorController;
use Illuminate\Support\Facades\Route;

Route::get('generator', [CrudGeneratorController::class, 'index']);
Route::post('generator/generate', [CrudGeneratorController::class, 'generate'])->name('generate-crud');